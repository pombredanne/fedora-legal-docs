////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Spec File License Tags

== Basic Policy

The spec file License tag consists of an enumeration of all licenses covering any code or other material contained in the corresponding *binary* RPM. This enumeration must take the form of an https://spdx.github.io/spdx-spec/v2.3/SPDX-license-expressions/[SPDX license expression]. No xref:license-field.adoc#_no_effective_license_analysis[further analysis as to the "effective" license] should be done. 

This policy means that there may be licenses covering files in the package source code that should not be reflected in the License tag because those files do not end up being compiled or otherwise included in the binary package. Common examples of this are Autoconf scripts and non-bundled test files.

Note that files in the package source code that are not compiled or otherwise included in the binary RPM must still be covered by a license xref:allowed-licenses.adoc[allowed] by Fedora, because Fedora is distributing the source code.

*Example:* The source code of package foo consists entirely of C files under the MIT license along with some Autoconf macros under the GNU All-permissive License (which corresponds to the SPDX short identifier FSFAP). The spec file would simply have:

``License: MIT``

and not `License: MIT AND FSFAP` because the Autoconf macros under FSFAP are not included in the binary package.

*Example:* The source code of package foo is licensed under LGPLv2.1-or-later except for some source files licensed under GPLv2-or-later which implement a feature that is not included in the Fedora build. The spec file would have:

``License: LGPL-2.1-or-later``

and not `License: LGPL-2.1-or-later AND GPL-2.0-or-later`.

Where there are multiple binary subpackages and the set of licenses associated with these multiple binary subpackages are not identical, there must be a distinct License tag for each subpackage.

Each license identifier in the SPDX expression forming the License tag must correspond to a license that is xref:allowed-licenses.adoc[allowed] by Fedora for the appropriate category of material. In rare cases Fedora may grant limited exceptions permitting otherwise not-allowed licenses for specific packages or under other specific conditions; this will be noted in the TOML file for the license in the Fedora License Data repository.

The remainder of this document provides some specific guidance on how to populate the License tag in accordance with this policy.
If you have further questions please ask on the https://lists.fedoraproject.org/admin/lists/legal.lists.fedoraproject.org/[Fedora legal list].

NOTE: If you encounter a new license that is not listed on the allowed or not-allowed license lists, please follow the xref:license-review-process.adoc[License Review Process].

== License Expressions

https://spdx.github.io/spdx-spec/v2.3/SPDX-license-expressions/[SPDX license expressions] provide a standardized way to represent the licensing terms typically found in open source software source code. 
A license expression can be a single license identifier found on the https://spdx.org/licenses[SPDX License List], e.g., `Apache-2.0`; a user-defined license reference prefixed by `LicenseRef-`; or some combination of license identifiers or https://spdx.org/licenses/exceptions-index.html[exceptions] constructed using a small set of defined operators (`AND`, `OR`, and `WITH`). 

=== License "WITH" Exception

If your package (or part of your package) is licensed under a license and also applies a permissive exception or additional permission (most commonly seen with licenses in the GPL family), the License tag must reflect this using `WITH` as a separator. Note that the combination of the license with the exception must be xref:allowed-licenses.adoc[allowed] by Fedora. 

*Example:* foo.rpm is built entirely from source code covered by "GPLv2 only" with the exception commonly known as the Classpath Exception, which corresponds to the SPDX exception identifier "Classpath-exception-2.0". The spec file must have:

``License: GPL-2.0-only WITH Classpath-exception-2.0``

Note that some license-related text accompanying a license or license notice may be one of the following:

* what we would consider a permissive exception or additional permission to an allowed license, which would be represented using a WITH expression;
* an additional restriction which needs to be treated as a distinct license and reviewed for overall allowability in Fedora; or
* a statement of interpretation that does not have to be treated as an exception.

In some cases the distinction between the first and third of these possibilities is mostly a matter of community tradition (some statements of interpretation come to be seen as license exceptions if they become well known or get reused by other projects).
For help in figuring out which of these situations applies, please create an issue following the xref:license-review-process.adoc[License Review Process].

NOTE: As of the adoption of SPDX identifiers in July 2022, the text for most exceptions allowed in Fedora was not yet captured to facilitate mapping to SPDX identifiers. Therefore, most exceptions will likely need to be xref:license-review-process.adoc[reviewed] and added to the Fedora license data. For more information on this transition, see xref:update-existing-packages.adoc[update existing packages].

=== Disjunctive "OR" License Choice (also known as "Dual" Licenses)

If your package (or part of your package) is licensed under a choice of two (or more) licenses, and each license is allowed for Fedora, the License tag must reflect this by using the `OR` operator. (Where, as is typical, there is a choice of two licenses, it is common in FOSS to call this "dual licensing", although that term is sometimes used to refer to different concepts.)

*Example:* foo.rpm is built entirely from source code dual-licensed under a choice of the Mozilla Public License 1.1 and GPLv2-or-later. The spec file must have:

``License: MPL-1.1 OR GPL-2.0-or-later``

However, if your package is licensed under a choice of two licenses and one is an allowed license and one is a not-allowed license, then the general rule is that the License tag must reflect the allowed license only. In this case you would not use an "OR" operator. In such situations you are encouraged to include a comment explaining how the code is licensed upstream.

See the section below on xref:license-field.adoc#_perl_packages[Perl packages] for an exception to this general rule.  

The ordering of the elements in an `OR` expression is arbitrary.

=== Conjunctive "AND" Licensing

If your package is built from files under multiple distinct licenses, then theLicense tag must reflect this by using the `AND` operator.  

*Example:* bar-utils.rpm is built from some files under the MIT License, some other files under LGPLv2.0-or-later, and one file under the three-clause BSD license. The spec file must have:

``License: MIT AND LGPL-2.0-or-later AND BSD-3-Clause``

A single license identifier should only appear once in an "AND" expression regardless of how many distinct source or binary components the corresponding license covers for the relevant binary RPM. 

*Example:* bar.rpm contains three executable utility programs. You’ve determined that two of them are each licensed under GPL version 2 only, while the third is licensed under the MIT license. The spec file would have: 

``License: GPL-2.0-only AND MIT``

It would not be `GPL-2.0-only AND GPL-2.0-only AND MIT`, even though from an orthodox GPL interpretation standpoint there are two separate GPL-licensed "Programs" in this package. 

*Example:* Same facts as bar.rpm above, but now bar.rpm also contains a fourth program licensed under GPL version 2 or later. Here the spec file would have:

``License: GPL-2.0-or-later AND GPL-2.0-only AND MIT`` 

That is, we treat the GPL version 2 or later and the GPL version 2 only parts as distinctly-licensed components, both of which must be reflected in the license expression. 

The ordering of the elements in an `AND` expression is arbitrary.

=== Combined Disjunctive and Conjunctive License Expressions

If your package is built from files under multiple distinct licenses, and some files are licensed under a choice of two (or more) licenses, then the License tag must include the appropriate `OR` and `AND` expressions. The `OR` subexpressions should be placed in parentheses (because SPDX follows the convention of giving `AND` higher precedence than `OR`). The license expression must reflect the disjunctive license choice even
if one or both of the license identifiers in the `OR` expression also appear separately in the composite license expression.

*Example:* baz-utils.rpm is built from some files under the MIT License, some other files under LGPLv2.1 or later, one file under the three-clause BSD license and one file which is dual licensed under MPL 1.1 and GPLv2 or later. The spec file would  have:

``License: MIT AND LGPL-2.1-or-later AND BSD-3-Clause AND (MPL-1.1 OR GPL-2.0-or-later)``

*Example:* baz.rpm contains three executable utility programs. You’ve determined that one is disjunctively dual-licensed under the choice of GPL version 3 or later or MPL version 1.1; one is licensed under GPL version 3 or later; and one is licensed under MIT. The spec file would be:

``License: (GPL-3.0-or-later OR MPL-1.1) AND GPL-3.0-or-later AND MIT`` 

Here we repeat GPL-3.0-or-later because for one binary component it appears as part of an `OR` subexpression. That is, `OR` expressions must be treated as though they were a single distinct license.

== Specific Considerations

=== Bundled or "vendored" dependencies

Some upstream projects bundle code copied from other upstream projects (a practice some communities sometimes call "vendoring"). In certain cases Fedora permits packaging of such projects including the bundled dependencies. In some other cases, a Fedora package may itself bundle dependencies that are not separately packaged in Fedora. In these cases, assuming the bundled dependencies are built or otherwise included in the packaged binary, the License tag must reflect the licenses covering the bundled code. 

*Example:* foo.rpm is built from source code that consists of upstream project source code under the Apache License 2.0 along with
several bundled dependencies, some of which are under the MIT license, one of which is (disjunctively) dual-licensed under the Apache License 2.0 and the MIT license, and one of which is under MPL 2.0). The spec file would have:

``License: Apache-2.0 AND MIT AND (Apache-2.0 OR MIT) AND MPL-2.0``

=== Rust packages

Since Rust applications are statically linked and contain code from all their dependencies, the License tag for the subpackage containing the built binary must contain the individual licenses of all dependencies in accordance with these guidelines.

////
The `rust2rpm` project provides a https://pagure.io/fedora-rust/rust2rpm/blob/main/f/tools/fedora-helper.py[helper script] that can be used to obtain a list of licenses of all real crate dependencies, excluding dependencies that are only required at build time or for running tests.
////

=== Perl packages

Perl 5 is presented upstream as being dual-licensed under the choice of GPL version 1-or-later (corresponding to the SPDX expression GPL-1.0-or-later) and the Artistic License 1.0 (more precisely, the license that corresponds to the SPDX short identifier Artistic-1.0-Perl). Many Perl modules say simply that they are licensed under "the same terms as Perl itself". Fedora treats this as an unambiguous reference to that well known Perl GPL|Artistic dual license.

The version of the Artistic License 1.0 used by Perl is not-allowed in Fedora. However, as an exception to the general rule that `OR` expressions should not be used if one of the licenses is not-allowed, packages containing Perl code that uses the Perl dual license can opt to use either:

``License: GPL-1.0-or-later``

or

``License: GPL-1.0-or-later OR Artistic-1.0-Perl``

=== Firmware

Any firmware license needs to be submitted for xref:license-review-process.adoc[review] under the xref:license-approval.adoc#_allowed_for_firmware[allowed for firmware] criteria and added accordingly to the Fedora license data, unless the license is already listed as an allowed or allowed-firmware license. 

Typical licenses designed for firmware may not be elgible for inclusion on the SPDX License List, in which case a `LicenseRef-` SPDX identifier will be assigned. 

For information on transitioning existing packages to the use of SPDX identifiers in the License tag, see xref:update-existing-packages.adoc[Update Existing Packages].

=== Public Domain

It is very common for portions of FOSS code to be covered by simple, informal public domain dedication language. For example, a source file might contain a comment at the top of the file saying: "This program is in the public domain." These statements do not actually cause the code they cover to enter the public domain; except in certain rare cases, they are more akin to extremely permissive licenses. 

Under the Callaway system, Fedora packages used  `Public Domain` in theLicense tag as an umbrella short name to represent all such public domain dedications. 

If you find a new package for inclusion in Fedora that has such a statement, then submit it for xref:license-review-process.adoc[review]. 

For more information on specific considerations relating to the transition to SPDX identifiers for existing packages, see xref:update-existing-packages.adoc[Update Existing Packages].

NOTE: It is likely that the guidelines on treatment of public domain dedications in the License tag will get refined as we gain further experience. 

== No “effective license” analysis

The License tag is meant to provide a simple enumeration of the licenses found in the source code that are reflected in the binary package. No further analysis should be done regarding what the "effective" license is, such as analysis based on theories of GPL interpretation or license compatibility or suppositions that “top-level” license files somehow negate different licenses appearing on individual source files. There is no agreed-upon set of criteria or rules under which one can make conclusions about “effective” licenses or reduce composite license expressions to something simpler. Here are some examples that illustrate this principle:

* foo.rpm contains an executable that is the result of compiling three source files, file1.c, file2.c and file3.c. You’ve determined that file1.c and file2.c are licensed under GPL version 2 or later, and that file2.c is licensed under the MIT license. The spec file would have `License: GPL-2.0-or-later AND MIT`, not simply `License: GPL-2.0-or-later`. You have to include “MIT” even though some might argue that the executable is in some sense globally or “effectively” licensed under the GPL (for example, by virtue of GPLv2 section 2), or that file3.c gets implicitly sublicensed under GPL version 2 or later. 
 
* Same facts as the previous example, but in this case the source code of foo includes a top-level COPYING file that you’ve determined contains the text of GPL version 2, and a README file that says “foo is licensed under GPL version 2 or later”. The License tag should still be `GPL-2.0-or-later AND MIT`. 

* Suppose a binary is built from source code licensed under the three-clause BSD license, but it dynamically links against a separately packaged library licensed under GPL version 3 or later. The spec file should have `License: BSD-3-clause`, not `License: GPL-3.0-or-later` or `License: BSD-3-Clause AND GPL-3.0-or-later`, even though one might argue as a matter of GPL interpretation that the two packages form a combined work that is as a whole subject to GPLv3-or-later. The same principle applies to packages containing code in languages like Perl or Python that may pull in separately packaged code in the same language at runtime.   


